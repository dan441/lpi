/*
  Write a program that opens an existing file for writing with the O_APPEND flag, and then seeks to the beginning of the file before writing some data. Where does the data appear in the file? Why?
*/

// strategy: open a tempfile, write 100 bytes of 0 to it. Close the file
// re-open with O_APPEND, and test writing 0xDEADBEEF at the beginning. Record file's position Close the file.
// re-open and find the position of 0xDEADBEEF - is this files position recorded originally?

// we want a tempfile that's created but not unlinked.

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include "tlpi_hdr.h"

#define PATH "/tmp/mostlyzerosXXXXXX"

/* returns file descriptor, modifies path */
int open_tempfile(char* path) {
  int fd = 0;

  path = mktemp(path);
  fd = open(path, O_RDWR | O_CREAT);

  if(fd == -1)
    errExit("open");

  return fd;
}


/* given a count of bytes (less than 1KB), and an open file fd, write count 0s to the file */
ssize_t write_zeros(size_t count, int fd) {
  ssize_t bytes_written = 0;
  char buffer[1024] = { 0 };

  bytes_written = write(fd, buffer, count);

  if (bytes_written < count)
    errExit("write zeros");

  return bytes_written;
}

/* given an open file fd, rewind to the beginning and write four bytes */
ssize_t write_deadbeef(int fd) {
  ssize_t bytes_written = 0;
  off_t position = 0;
  uint32_t deadbeef = 0xDEADBEEF;

  position = lseek(fd, position, SEEK_SET);
  bytes_written = write(fd, &deadbeef, 4);

  if (bytes_written < 4)
    errExit("write deadbeef");

  return bytes_written;
}

int main() {
  int fd;
  ssize_t written;
  char path[] = PATH;

  fd = open_tempfile(path);
  puts(path);
  written = write_zeros(100, fd);
  close(fd);

  fd = open(path, O_RDWR | O_APPEND);
  written = write_deadbeef(fd);
  close(fd);

  return 0;
}
//  written = write_zeros(100, fd);

  /* path was modified in open_tempfile which is kind of gross... */
