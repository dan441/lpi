#include <unistd.h>
#include <stdio.h>

int main() {
  char buf[256]; // hope that's big enough
  // we need to call this for side-effect, as it returns size_t, not char *
  confstr(_CS_GNU_LIBC_VERSION, buf, 256);

  printf("%s\n", buf);

  return 0;
}
