
/*
Write a program like cp that, when used to copy a regular file that
contains holes (sequences of null bytes), also creates corresponding
holes in the target file.
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// use -I../lib/ to find this:
#include "tlpi_hdr.h"


#ifndef BUF_SIZE
#define BUF_SIZE 1024
#endif

int zeroes(char bytes[], ssize_t size) {
  ssize_t i;
  for(i = 0; i < size; i++)
    if (bytes[i] != 0)
      return 0;
  return 1;
}


int
copy_contents(int from, int to)
{
  char buffer[BUF_SIZE];
  ssize_t bytes_read;

  while((bytes_read = read(from, buffer, BUF_SIZE)) > 0) {
    if (zeroes(buffer, bytes_read) == 1)
      lseek(to, bytes_read, SEEK_CUR);
    else if(bytes_read != write(to, buffer, bytes_read))
      fatal("couldn't write whole buffer");
  }

  return bytes_read;
}


mode_t permissions() {
  /* rw-rw-rw- */
  return ( S_IRUSR | S_IWUSR |
	   S_IRGRP | S_IWGRP |
	   S_IROTH | S_IWOTH);
}

int open_flags() {
  return O_CREAT | O_WRONLY | O_TRUNC;
}

int main(int argc, char *argv[]) {
  int inputFd, outputFd;
  int bytes_copied = 0;

  if (argc != 3)
    usageErr("%s infile outsfile\n", argv[0]);

  inputFd = open(argv[1], O_RDONLY);
  if (inputFd == -1)
    errExit("opening file %s", argv[1]);

  outputFd = open(argv[2], open_flags(), permissions());
  if (outputFd == -1)
    errExit("opening file %s", argv[2]);

  bytes_copied = copy_contents(inputFd, outputFd);

  if(bytes_copied == -1)
    errExit("read");

  if(close(inputFd) == -1)
    errExit("close %s", argv[1]);
  if(close(outputFd) == -1)
    errExit("close %s", argv[2]);

  exit(EXIT_SUCCESS);
}
