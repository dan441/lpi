#ifndef GET_NUM_H
#define GET_NUM_H

#define GN_NONNEG 01 /*Value must be >= 0 */
#define GN_GT_0 02 /* value must be > 0 */
#define GN_ANY_BASE 0100 /* can be any base, like strtol */
#define GN_BASE_8 0200 /* value is octal */
#define GN_BASE_16 0400 /* value is hexadecimal */
/*
  The getInt() and getLong() functions convert the string pointed to
  by arg to an int or a long, respectively. If arg doesn’t contain a
  valid integer string (i.e., only digits and the characters + and
  -), then these functions print an error message and terminate the
  program.

  Flags are used to set alternate bases or restrict the range to be non-negative

  If the name argument is non-NULL, it should contain a string
  identifying the argument in arg. This string is included as part of
  any error message displayed by these functions.
*/

int getInt(const char *arg, int flags, const char *name);
long getLong(const char *arg, int flags, const char *name);

#endif
