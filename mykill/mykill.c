#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

// the actual call
int mykill(int pid) {
  // 0 and -1 are special, broadcast to the process group, and broadcast to all possible processes
  if(pid < 1) {
    exit(1);
  }
  // assuming all went well, cast integer pid to pid_t and call kill
  return kill((pid_t) pid, SIGKILL);
}

int main(int argc, char *argv[]) {
  if(argc < 2 ){
    // usage
    printf("%s PID\n", argv[0]);
    exit(0);
  }

  int pid = strtol(argv[1], NULL, 10);

  // strtol returns 0 on invalid input, by mykill() guards against that and exits early
  /*
    from man 2 kill:
    RETURN VALUE
    On success (at least one signal was sent), zero is returned.  On error,
    -1 is returned, and errno is set appropriately.
  */
  return mykill( pid);
  }
}
