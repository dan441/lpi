#include <unistd.h>
#include <fcntl.h>

int main() {
  char * pathname = "/tmp/myfile";

  // not actually a sparse file...
  int flags = O_WRONLY | O_CREAT | O_TRUNC;
  int fd = 0;
  off_t size = 102400; // lets go big with 1 KB!
  ssize_t bytes = 0;

  fd = open(pathname, flags, 0x644);

  if ( fd == -1)
    return 1;

  // does seeking forward 1KB make the file that large? - no - /tmp/myfile is 0 bytes if I don't write anything.
  size = lseek(fd, size, SEEK_END);
  if(size == -1)
    return 2;

  // lets write a byte! at the end of the file.
  bytes = write(fd, (void *) pathname, 1);
  if(bytes != 1)
    return 3;

  close(fd);

  return 0;
}
// and now /tmp/myfile is 1025 bytes long, 1024 nulls followed by a slash (0x2f)
