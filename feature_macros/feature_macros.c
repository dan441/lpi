#include <stdio.h>
#include <stdlib.h>

void posix_source_version() {
#ifdef _POSIX_SOURCE
  printf("_POSIX_SOURCE is defined, value %d\n", _POSIX_SOURCE);
#else
  printf("_POSIX_SOURCE is not defined\n");
#endif
}

void posix_c_source_version() {
  // posix_c_source_version is long int, not int, -Wall pointed this out
#ifdef _POSIX_C_SOURCE
  printf("_POSIX_C_SOURCE is defined, value %ld\n", _POSIX_C_SOURCE);
#else
  printf("_POSIX_C_SOURCE is not defined\n");
#endif
}

void bsd_source_version() {
#ifdef _BSD_SOURCE
  printf("_BSD_SOURCE is defined, value %d\n", _BSD_SOURCE);
#else
  printf("_BSD_SOURCE is not defined\n");
#endif
}

void svid_source_version() {
#ifdef _SVID_SOURCE
  printf("_SVID_SOURCE is defined, value %d\n", _SVID_SOURCE);
#else
  printf("_SVID_SOURCE is not defined\n");
#endif
}

void xopen_source_version() {
#ifdef _XOPEN_SOURCE
  printf("_XOPEN_SOURCE is defined, value %d\n", _XOPEN_SOURCE);
#else
  printf("_XOPEN_SOURCE is not defined\n");
#endif
}

void gnu_source_version() {
#ifdef _GNU_SOURCE
  printf("_GNU_SOURCE is defined, value %d\n", _GNU_SOURCE);
#else
  printf("_GNU_SOURCE is not defined\n");
#endif
}


int main() {
  posix_source_version();
  posix_c_source_version();
  bsd_source_version();
  svid_source_version();
  xopen_source_version();
  gnu_source_version();

  return 0;
}
