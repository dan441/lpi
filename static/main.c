#include <stdio.h>

int identity(int);
int negation(int);

int main() {
  int i = 2;
  printf("i is %d, -i is %d\n",
	 identity(i),
	 negation(i));
  return 0;
}
